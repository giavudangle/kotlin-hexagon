package com.vudang.kotlin.hexagon.adapter.config

import lombok.extern.slf4j.Slf4j
import org.axonframework.commandhandling.SimpleCommandBus
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.commandhandling.gateway.DefaultCommandGateway
import org.axonframework.queryhandling.DefaultQueryGateway
import org.axonframework.queryhandling.SimpleQueryBus
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@Slf4j
open class AxonConfiguration {
  @Bean
  open fun simpleCommandBus(): SimpleCommandBus {
    return SimpleCommandBus.builder().build();
  }

  @Bean
  open fun simpleQueryBus(): SimpleQueryBus? {
    return SimpleQueryBus.builder().build();
  }

  @Bean
  open fun commandGateway(
    simpleCommandBus: SimpleCommandBus,
  ): CommandGateway {
    return DefaultCommandGateway.builder().commandBus(simpleCommandBus).build()
  }

  @Bean
  open fun queryGateway(simpleQueryBus: SimpleQueryBus): DefaultQueryGateway? {
    return DefaultQueryGateway.builder().queryBus(simpleQueryBus).build()
  }
}
