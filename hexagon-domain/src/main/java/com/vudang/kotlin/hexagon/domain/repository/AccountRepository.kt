package com.vudang.kotlin.hexagon.domain.repository

import com.vudang.kotlin.hexagon.domain.entity.Account

interface AccountRepository : BaseRepository<Account> {}
