package com.vudang.kotlin.hexagon.adapter.config

import org.axonframework.eventhandling.EventBus
import org.axonframework.eventhandling.GenericEventMessage
import org.axonframework.eventhandling.gateway.DefaultEventGateway
import org.axonframework.eventhandling.gateway.EventGateway

class HexagonEventGateway(eventBus: EventBus) : DefaultEventGateway(DefaultEventGateway.builder().eventBus(eventBus)) {

    override fun publish(event: Any) {
        // this.eventBus.publish(processInterceptors<>(GenericEventMessage.asEventMessage(event).))
    }
}