package com.vudang.kotlin.hexagon.adapter.dto

import com.vudang.kotlin.hexagon.domain.entity.Account

data class AccountDTO(var account: Account)
