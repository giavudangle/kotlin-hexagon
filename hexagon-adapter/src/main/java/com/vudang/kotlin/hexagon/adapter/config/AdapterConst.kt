package com.vudang.kotlin.hexagon.adapter.config

object AdapterConst {
  public const val CONTEXT_KEY = "HEXAGON_CONTEXT_KEY"
}
